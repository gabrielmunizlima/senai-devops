environment{
def customImage    
}
stage 'Clone'
 node('') {
  deleteDir()
  checkout scm
 }
stage 'Build'
 node('') {
  sh 'ls -la /usr/local/bin/'
  sh 'mvn clean install -DskipTests'
 }
stage 'Test'
 node('') {
  sh 'mvn test'
 }
stage 'Package'
 node('') {
  docker.withRegistry('', 'credentials-docker'){
  customImage = docker.build("gabrielmuniz95/senaiapp:${env.BUILD_ID}")
  }
 }
stage 'Publish'
 node('') {
  customImage.push()
}
stage 'Deploy'
 node('') {
 
  sh 'docker ps -q --filter "label=senaiappinfra" > commandResult'
  def dockerExec = readFile('commandResult').trim()
  
  if(dockerExec != null && dockerExec != ""){
  sh 'docker rm -f $(docker ps -q --filter "label=senaiappinfra")'
  }
  sh '/usr/local/bin/docker-compose -f factor-app.yaml up -d'
}
stage 'Catalog'
	node(''){	
	def numberVersion = "${env.BUILD_ID}"
 	def imageVersion = 'gabrielmuniz95/senaiapp:' + numberVersion
 	
 	def value = 'curl -X POST  https://trello.com/1/cards' +
 		' --header \"Content-Type: application/json\" ' +
		' --data \'{' + 
		' \"name\": \"' + imageVersion + '\", ' +
		'\"desc\": \"Card de Implantação\", ' +
		'\"key\": \"901edc98ac1ade6b6f5c85686c17d2bd\", ' +
		'\"token\": \"bc4fd7582a3b9bd715790945d92e42ad3d1930f3be4d6c17a18638c33852f245\", ' +
		'\"pos\":\"top\", ' +
		'\"idList\": \"5c830c26ba15fa41db39ba76\" ' +
		'}\''
		
	echo value
	sh value
	}