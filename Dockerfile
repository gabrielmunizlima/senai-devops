FROM openjdk:8-jre-alpine
VOLUME /tmp
ADD /target/*.jar /root/my-app.jar
ENV DATASOURCE_URL=
ENV SPRING_RABBITMQ_HOST=
ENV REDIS_HOST=
CMD ["java","-jar","/root/my-app.jar"]