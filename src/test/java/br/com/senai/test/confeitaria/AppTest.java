package br.com.senai.test.confeitaria;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import br.com.senai.confeitaria.App;

public class AppTest 
   
{
	@Test
     public void start() {
    	 App app = new App();
    	 assertTrue(app.start().length() > 0);
     }
}
