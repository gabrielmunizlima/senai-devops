package br.com.senai.confeitaria.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Bolo implements IDomain, Serializable {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private double precoKg;
	@ManyToOne(cascade=CascadeType.PERSIST)
	private Sabor sabor;
	private boolean disponivel;
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Ingrediente> ingredientes;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
 
	public double getPrecoKg() {
		return precoKg;
	}
	public void setPrecoKg(double precoKg) {
		this.precoKg = precoKg;
	}
	public Sabor getSabor() {
		return sabor;
	}
	public void setSabor(Sabor sabor) {
		this.sabor = sabor;
	}
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}
	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	public boolean isDisponivel() {
		return disponivel;
	}
	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
	
	
}
