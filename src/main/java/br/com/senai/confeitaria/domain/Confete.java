package br.com.senai.confeitaria.domain;

import javax.persistence.DiscriminatorColumn;
 
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name="idConfete")
@DiscriminatorColumn(name = "colorido")
public class Confete extends Ingrediente {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean colorido;

	public boolean isColorido() {
		return colorido;
	}

	public void setColorido(boolean colorido) {
		this.colorido = colorido;
	}

}
