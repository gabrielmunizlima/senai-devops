package br.com.senai.confeitaria.service;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
  
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Component
public class PedidoConsumer {
 
	Logger logger = LoggerFactory.getLogger(PedidoConsumer.class);
	
    @RabbitListener(queues = {"${queue.pedido.name}"})
    public void receive(@Payload String pedido) {
    	try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			logger.error("problema na thread: ", e);
			e.printStackTrace();
		}
    	logger.info("Pedido realizado: " + pedido);
    }
}
