package br.com.senai.confeitaria.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.stereotype.Service;
 
import br.com.senai.confeitaria.domain.Bolo;
import br.com.senai.confeitaria.domain.Ingrediente;
import br.com.senai.confeitaria.domain.Sabor;
import br.com.senai.confeitaria.repository.BoloRepository;

@Service
public class BoloService {
	@Autowired
	private BoloRepository boloRepo;
	
	public void save(Bolo bolo) {
		boloRepo.save(bolo);
		 bolo.setIngredientes( new ArrayList(bolo.getIngredientes())) ;
		bolo.setSabor((Sabor) Hibernate.unproxy(bolo.getSabor()));
		
	}
	 
	public List<Bolo> find(boolean disponivel) {
		return boloRepo.findByDisponivel(disponivel);
	}
}
