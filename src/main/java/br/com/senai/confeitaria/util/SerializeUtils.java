package br.com.senai.confeitaria.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializeUtils {

	public static Object deserialize(byte[] bytes) throws IOException {

		Object result = null;

		if (isEmpty(bytes)) {
			return null;
		}

		ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes);

		ObjectInputStream objectInputStream = new ObjectInputStream(byteStream);
		try {
			result = objectInputStream.readObject();
		} catch (ClassNotFoundException ex) {
			throw new IOException("Failed to deserialize object type", ex);
		}

		return result;
	}

	public static boolean isEmpty(byte[] data) {
		return (data == null || data.length == 0);
	}

	public static byte[] serialize(Object object) throws IOException {

		byte[] result = null;

		if (object == null) {
			return new byte[0];
		}

		ByteArrayOutputStream byteStream = new ByteArrayOutputStream(128);

		if (!(object instanceof Serializable)) {
			throw new IllegalArgumentException(SerializeUtils.class.getSimpleName() + " classe ["
					+ object.getClass().getName() + "] não é serializada ");
		}
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteStream);
		objectOutputStream.writeObject(object);
		objectOutputStream.flush();
		result = byteStream.toByteArray();

		return result;
	}
}
