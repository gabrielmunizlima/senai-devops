package br.com.senai.confeitaria.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.senai.confeitaria.dao.BoloDAO;
import br.com.senai.confeitaria.dao.RedisManager;
import br.com.senai.confeitaria.domain.Bolo;
import br.com.senai.confeitaria.domain.ErroMsg;
import br.com.senai.confeitaria.service.BoloService;
import br.com.senai.confeitaria.service.PedidoQueueSender;
import br.com.senai.confeitaria.util.RedisProperties;

@RestController
@RequestMapping("/bolo")
public class ControllerBolo {
	@Autowired
	private RedisProperties props;
	@Autowired
	private BoloService boloService;
	@Autowired
	private PedidoQueueSender pedido;

	private BoloDAO boloDao;

	public ControllerBolo() {
		boloDao = new BoloDAO();
	}

	@GetMapping
	public ResponseEntity<List<Bolo>> obterBolos(
			@RequestParam(value = "apenasDisponiveis", defaultValue = "false") boolean apenasDisponiveis) {
		return new ResponseEntity<>(boloService.find(apenasDisponiveis), HttpStatus.OK);

	}
	
	@GetMapping(value = "/examples")
	public ResponseEntity<List<Bolo>> examples(
			@RequestParam(value = "apenasDisponiveis", defaultValue = "false") boolean apenasDisponiveis) {
		return new ResponseEntity<>(boloDao.consultar(false), HttpStatus.OK);

	}
	@PutMapping
	public ResponseEntity<Object> atualizarBolo(@RequestBody Bolo bolo) {
		try {
			return new ResponseEntity<>(boloDao.atualizar(bolo), HttpStatus.OK);
		} catch (FileNotFoundException e) {
			return new ResponseEntity<>(new ErroMsg("Id do bolo informado não localizado!"), HttpStatus.NOT_FOUND);
		}

	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Object> removerBolo(@PathVariable long id) {
		try {
			boloDao.remover(id);
			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (FileNotFoundException e) {
			return new ResponseEntity<>(new ErroMsg("Id do bolo informado não localizado!"), HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping
	public ResponseEntity<Object> createBoloCache(@RequestBody Bolo bolo) throws IOException {
		RedisManager redis = new RedisManager(props);
		try {
			boloService.save(bolo);
			redis.set(bolo);
			return new ResponseEntity<>(bolo, HttpStatus.CREATED);
		} catch (IOException e) {
			return new ResponseEntity<>(new ErroMsg(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value = "/last")
	public ResponseEntity<Object> getlastBolo() throws IOException {
		RedisManager redis = new RedisManager(props);
		Bolo bolo = (Bolo) redis.get(Bolo.class.getSimpleName());
		return new ResponseEntity<>(bolo, HttpStatus.OK);
	}

	@GetMapping(value = "/pedido")
	public ResponseEntity<Object> getPedido(@RequestParam(value = "key", defaultValue = "bolo123") String key) {
		pedido.send(key);
		return new ResponseEntity<>("bolo solicitado com sucesso!", HttpStatus.OK);
	}

}
