package br.com.senai.confeitaria;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.MessageFormat;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hello world!
 *
 */
@RestController
@SpringBootApplication
@EnableAutoConfiguration
public class App {

	@Value("${queue.pedido.name}")
	private String pedidoQueue;

	private static final Log logger = LogFactory.getLog(SpringApplication.class);

	public static void main(String[] args) throws IOException {
		// if (createArchiveDisk()) {
		SpringApplication.run(App.class, args);
		// }
	}

	public static boolean createArchiveDisk() throws IOException {
		long minSpaceDisk = 30000000000l;
		long spaceDisk = new File("/").getFreeSpace();
		logger.info(MessageFormat.format("space free {0}", spaceDisk));

		if (spaceDisk > minSpaceDisk) {
			logger.info("creating logs...");
			for (int i = 1; i <= 40; i++) {
				RandomAccessFile f = new RandomAccessFile("log" + i, "rw");
				f.setLength(1024 * 1024 * 1024);
				f.writeBytes("fdisdisjdsjdskdjskdjsdskdjskdjksjd");
				f.close();
			}
			logger.info("create logs success");
			logger.info(MessageFormat.format("space free {0}", new File("/").getFreeSpace()));

			return true;
		} else {
			logger.error("Sorry... Insuficient space disk");
			return false;
		}
	}

	@GetMapping(value = "/home")
	public String start() {
		return "aplicação iniciada com sucesso";
	}

	@Bean
	public Queue queue() {
		return new Queue(pedidoQueue, true);
	}

}
