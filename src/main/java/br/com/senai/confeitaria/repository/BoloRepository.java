package br.com.senai.confeitaria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import br.com.senai.confeitaria.domain.Bolo;

public interface BoloRepository  extends JpaRepository<Bolo, Integer>  {

	public List<Bolo> findByDisponivel(boolean disponivel);
	
}
