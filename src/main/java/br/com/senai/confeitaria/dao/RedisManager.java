package br.com.senai.confeitaria.dao;

import java.io.IOException;
import java.util.Set;
import org.springframework.stereotype.Component;
import br.com.senai.confeitaria.domain.IDomain;
import br.com.senai.confeitaria.util.RedisProperties;
import br.com.senai.confeitaria.util.SerializeUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Component
public class RedisManager {
	private RedisProperties props;
	private int expire = 0;
	private int timeout = 0;

	private static JedisPool jedisPool = null;

	public RedisManager(RedisProperties props) {
		this.props = props;
		init();
	}

	private void init() {
		if (jedisPool == null) {
			if (props.getPassword() != null && !"".equals(props.getPassword())) {
				jedisPool = new JedisPool(new JedisPoolConfig(), props.getHost(), props.getPort(), timeout,
						props.getPassword());
			} else if (timeout != 0) {
				jedisPool = new JedisPool(new JedisPoolConfig(), props.getHost(), props.getPort(), timeout);
			} else {
				jedisPool = new JedisPool(new JedisPoolConfig(), props.getHost(), props.getPort());
			}

		}
	}

	public Object get(String className) throws IOException {
		byte[] key = SerializeUtils.serialize(className);
		byte[] value = get(key);
		return SerializeUtils.deserialize(value);
	}

	public byte[] get(byte[] key) {
		byte[] value = null;
		Jedis jedis = jedisPool.getResource();
		return jedis.get(key);
	}

	public void set(IDomain domain) throws IOException {
		byte[] key = SerializeUtils.serialize(domain.getClass().getSimpleName());
		byte[] value = SerializeUtils.serialize(domain);

		set(key, value);
	}

	public byte[] set(byte[] key, byte[] value) {
		Jedis jedis = jedisPool.getResource();
		jedis.set(key, value);
		return value;
	}

	/**
	 * set
	 * 
	 * @param key
	 * @param value
	 * @param expire
	 * @return
	 */
	public byte[] set(byte[] key, byte[] value, int expire) {
		Jedis jedis = jedisPool.getResource();
		jedis.set(key, value);
		if (expire != 0) {
			jedis.expire(key, expire);
		}

		return value;
	}

	/**
	 * del
	 * 
	 * @param key
	 */
	public void del(byte[] key) {
		Jedis jedis = jedisPool.getResource();
		jedis.del(key);

	}

	/**
	 * flush
	 */
	public void flushDB() {
		Jedis jedis = jedisPool.getResource();
		jedis.flushDB();

	}

	/**
	 * size
	 */
	public Long dbSize() {
		Long dbSize = 0L;
		Jedis jedis = jedisPool.getResource();
		dbSize = jedis.dbSize();

		return dbSize;
	}

	/**
	 * keys
	 * 
	 * @param regex
	 * @return
	 */
	public Set<byte[]> keys(String pattern) {
		Set<byte[]> keys = null;
		Jedis jedis = jedisPool.getResource();
		keys = jedis.keys(pattern.getBytes());
		return keys;
	}

	public int getExpire() {
		return expire;
	}

	public void setExpire(int expire) {
		this.expire = expire;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}
