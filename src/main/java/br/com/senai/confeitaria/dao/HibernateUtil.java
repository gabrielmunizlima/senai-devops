package br.com.senai.confeitaria.dao;

import java.io.Closeable;
import java.io.IOException;
import java.text.MessageFormat;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Service;

import br.com.senai.confeitaria.domain.Bolo;
import br.com.senai.confeitaria.domain.IDomain;

@Service
public class HibernateUtil {
	@PersistenceContext
	private EntityManager manager;

	public HibernateUtil() {
	 
	}

	
	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	 

	public String testConnection() {
		// tityManager manager = factory.createEntityManager();

		Query query = manager.createNativeQuery("select * from " + Bolo.class.getSimpleName());

		String ret = MessageFormat.format("existem {0} itens de bolo cadastrados", query.getResultList().size());

		manager.close();

		return ret;
	}

	public IDomain save(IDomain domain) {
		// ntityManager manager = factory.createEntityManager();
		manager.getTransaction().begin();

		try {
			manager.persist(domain);
			manager.getTransaction().commit();
		} catch (Exception e) {
			manager.getTransaction().rollback();
		} finally {
			manager.close();
		}

		return domain;
	}

}
