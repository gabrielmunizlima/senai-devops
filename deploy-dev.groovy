import groovy.json.JsonSlurper

@NonCPS
def jsonParse(def json) {
    new groovy.json.JsonSlurperClassic().parseText(json)
}

environment{
 def idCard
}
stage 'DEPLOY'
 node('') {
    
    sh "curl -X GET https://trello.com/1/lists/5c830c34f835eb445e4996b4/cards?fields=name > result";
    def output=readFile('result').trim()
    echo "output=$output"
    def json = jsonParse(output)

    def cards = (String[]) json.collect({it.name})
   
    
    if(cards.length  > 0){
    def idCards = (String[]) json.collect({it.id})
        
    print cards[0]
    print idCards[0]
    
    idCard = (String) idCards[0]
    
     def deployment_name = 'my-app'
     @NonCPS
     def imageVersion = (String) cards[0]
    def namespace = 'development'
    def urlKubernetes = 'https://192.168.0.75:6443/apis/extensions/v1beta1/namespaces/' +
    namespace + '/deployments/' + deployment_name
   echo urlKubernetes  
   
   def value = 'curl -X PATCH ' + urlKubernetes +
    ' --header \"Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4taDJ6cTkiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImYyODEwMDhkLTNhMjgtMTFlOS1hZDlkLTQ4NGQ3ZWZiZGQxNSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.jmvKAFXJ7lAgOY5CEDg8X4s9ibbA2Tlh9_iWtUKGobhcZ98nWYraorxUk_i4HRmewoQ8dHEDkWUpVreH0w29v67TSywTyDkzGAzo2F97pmIcdt02Azgx_sjf3EiUGz-MsW5JpimO3ZjfXH0fxn4hhf5aKh9GJF1CjvV7s3BxDGw9P7t7yKkCpVb4KeyH_L8V7rGJY-Kqo1I0uFQJ_DJy4ESM7S6tjpf7BoSVqa64fKI6ffGutsPnKRTpP3HxKTDa7x2M5ozEkQC26ZHT5wrz98t6GnPxZ_HuR-er2u2rmtEWS8bc07kOeX47N6bqqdInTVzRy5r39CxMyEYhhX23KA\" --insecure ' +
    ' --header \"Content-Type: application/json-patch+json\" ' +
    ' --data \'[{ \"op\": \"replace\", \"path\": \"/spec/template/spec/containers/0/image\", \"value\": \"' +
    imageVersion + '\"}]\''

    echo value
    sh value
    }
    else{
       currentBuild.result = 'ABORTED'
        error('nenhum card de implantação identificado')
    }
    
}
stage 'DEPLOY-UPDATE STATUS'
 node('') {
    def idListTo = "5c883611e918f326613d9bdd"
	def key = "901edc98ac1ade6b6f5c85686c17d2bd"
	def token = "bc4fd7582a3b9bd715790945d92e42ad3d1930f3be4d6c17a18638c33852f245"

  def url =  'curl -X PUT \"https://trello.com/1/cards/' +
  idCard + 
  '/idList?value=' + 
  idListTo +
  '&key=' +
  key +
  '&token=' + 
  token + '\"'

 sh url
 
}